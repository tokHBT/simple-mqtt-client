# Simple MQTT Client

## Install
```
$ yarn 
```

## Run it
```
$ yarn start
```

## Change Configuration

Just change the variable in the `config.yaml` file:
```yaml
# config.yaml

url: "SERVER_URL"
topics:
  - "TOPIC_1"
  - "TOPIC_2"
  - ...
```
