

const yaml = require('js-yaml');
const fs   = require('fs');
const mqtt = require('mqtt');

console.log("Simple MQTT Client is running.");

var url = "";
var topics = [];

// Read config
try {
  const config = yaml.load(fs.readFileSync('./config.yaml', 'utf8'));
  console.log("Config loaded\n")

  if (config.url != null) {
        url = config.url
  }

  if (config.topics != null && config.topics.length > 0) {
        topics = config.topics;
  }
} catch (e) {
  console.log(e);
}

var client  = mqtt.connect(url);

client.on('connect', function () {
  client.subscribe(topics, function (err) {
    if (!err) {
      console.log("Connected to " + url + "\n");

      topics.forEach( topic => console.log("Subscribed to topic: " + topic) );

      console.log("")
    }
  })
})

var counter = 0;

client.on('message', function(topic, message, packet) {
      counter++;
      msg = JSON.parse(message);

      console.log("Message " + counter + ":\tfrom topic '" + topic + "':\t" + msg['@iot.selfLink'] + "" );
});
